package com.javainterviewpoint.Controller;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

/*import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;*/
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.javainterviewpoint.DataAccess.AuthorDAO;
import com.javainterviewpoint.DataAccess.BookDAO;
import com.javainterviewpoint.DataAccess.BookGenresDAO;
import com.javainterviewpoint.DataAccess.DataSourceGenerator;
import com.javainterviewpoint.DataAccess.GenreDAO;
import com.javainterviewpoint.DataAccess.SerializationDAO;
import com.javainterviewpoint.DataAccess.StatusDAO;
import com.javainterviewpoint.DataAccess.UserDAO;
import com.javainterviewpoint.Model.Author;
import com.javainterviewpoint.Model.Book;
import com.javainterviewpoint.Model.BookGenres;
import com.javainterviewpoint.Model.DetailedBook;
import com.javainterviewpoint.Model.Genre;
import com.javainterviewpoint.Model.Serialization;
import com.javainterviewpoint.Model.Status;
import com.javainterviewpoint.Model.User;
import com.javainterviewpoint.Recommendation.RecommendationSystem;
import com.javainterviewpoint.Recommendation.UserProfile;

import org.springframework.ui.Model;

@Controller
public class MainController {
	
	private DataSourceGenerator dataSource = new DataSourceGenerator();
	private UserDAO userAccess = new UserDAO(dataSource);
	private AuthorDAO authorAccess = new AuthorDAO(dataSource);
	private GenreDAO genresAccess = new GenreDAO(dataSource);
	private BookDAO bookAccess = new BookDAO(dataSource);
	private StatusDAO statusAccess = new StatusDAO(dataSource);
	private SerializationDAO serAccess = new SerializationDAO(dataSource);
	private BookGenresDAO genresBook = new BookGenresDAO(dataSource);
	private RecommendationSystem recommendationSystem = new RecommendationSystem();
	DecimalFormat df = new DecimalFormat("#.##");
	
	private void generateRecommendations(int idUser, Model model) {
		
		List<DetailedBook> readBooks = bookAccess.getReadBooks(idUser);
		List<DetailedBook> unreadBooks = bookAccess.getUnreadBooks(userAccess, idUser);
		
		readBooks.stream().forEach(book ->{
			
			book.setGenres(genresAccess.getBookGenresAsList(book.getIdBook()));
		
		});
		
		unreadBooks.stream().forEach(book ->{
			
			book.setScore(Float.valueOf(df.format(book.getScore())));
			book.setGenres(genresAccess.getBookGenresAsList(book.getIdBook()));
		
		});
		
		UserProfile userProfile = recommendationSystem.generateUserProfile(readBooks);
		List<DetailedBook> recommendedBooks = recommendationSystem.generateRecommendations(unreadBooks, userProfile);
		
		model.addAttribute("books", recommendedBooks);
	}
	
	@RequestMapping("/login")
	public String toLogin() {
		
		return "login";
	}
	
	@RequestMapping("/")
	public String index(HttpServletRequest request) {
		
		System.out.println(request.getRemoteAddr());
		
		return "login";
	}
	
	@RequestMapping("/register")
	public String toRegister() {
		
		return "register";
	}
	
	@RequestMapping("/registerUser")
	public String register(@RequestParam("Uname") String userName, @RequestParam("Pass") String password, @RequestParam("PassConf") String confPass, Model model, HttpSession session) {
		
		if(!password.equals(confPass))
			return "register";
		
		List<User> result = userAccess.findByField("name", userName);
		
		if(!result.isEmpty())
			return "register";
	
		int idUser = userAccess.getMaxId();
		
		userAccess.addUser(new User(idUser + 1, userName, password, false));
		
		model.addAttribute("userId", idUser + 1);
		session.setAttribute("userId", idUser + 1);
		
		session.setAttribute("isAdmin", false);
		
		return "user";
	}
	
	@RequestMapping("/loginUser")
	public String login(HttpSession session, @RequestParam("username") String username, @RequestParam("password") String password, Model model) {
		
		User user = userAccess.getUser(username, password);
		
		if (user == null || username.equals("") || password.equals("")) {
			
			return "login";
		}
		
		session.setAttribute("userId", user.getIdUser());
		
		if(user.getIsAdmin()) {
			
			session.setAttribute("isAdmin", true);
			return "admin";
			
		}else {
			
			generateRecommendations(user.getIdUser(), model);
			session.setAttribute("isAdmin", false);
			
			return "user";
		}
	}
	
	@RequestMapping("/author")
	public String toAuthor() {
		
		return "author";
	}
	
	@RequestMapping("/genre")
	public String toGenre() {
		
		return "genre";
	}
	
	@RequestMapping("/serialization")
	public String toSer() {
		
		return "serialization";
	}
	
	@ModelAttribute("detailedBook")
	public DetailedBook getDetailedBook() {
		
		return new DetailedBook();
	}
	
	@ModelAttribute("genresList")
	public List<String> getGenresList(){
		
		List<Genre> genres = genresAccess.selectAll();
		
		return genres.stream().map(x -> x.getName())
				.collect(Collectors.toList());
	}
	
	@ModelAttribute("serList")
	public List<String> getSerList(){
		
		List<Serialization> sers = serAccess.selectAll();
		
		return sers.stream().map(x -> x.getName())
				.collect(Collectors.toList());
	}
	
	@ModelAttribute("authorList")
	public List<String> getAuthorList(){
		
		List<Author> authors = authorAccess.selectAll();
		
		return authors.stream().map(x -> x.getName())
				.collect(Collectors.toList());
	}
	
	@ModelAttribute("statusList")
	public List<String> getStatuses(){
		
		List<Status> statuses = statusAccess.selectAll();
		
		return statuses.stream().map(x -> x.getName())
				.collect(Collectors.toList());
	}
	
	@RequestMapping("/book")
	public String toBook(Model model) {
		
		return "book";
	}
	
	@RequestMapping("/addAuthor")
	public String addAuthor(@RequestParam("fname") String name) {
		
		if(!authorAccess.findByField("name", name).isEmpty())
			return "error";
		
		int maxId = authorAccess.getMaxId();
		Author toBeAdded = new Author(maxId + 1, name);

		authorAccess.insert(toBeAdded);
		
		return "author";
	}
	
	@RequestMapping("/addGenre")
	public String addGenre(@RequestParam("fname") String name) {
		
		if(!genresAccess.findByField("name", name).isEmpty())
			return "error";
		
		int maxId = genresAccess.getMaxId();
		Genre toBeAdded = new Genre(maxId + 1, name);

		genresAccess.insert(toBeAdded);
		
		return "genre";
	}
	
	@RequestMapping("/addSerialization")
	public String addSerialization(@RequestParam("fname") String name) {
		
		if(!serAccess.findByField("name", name).isEmpty())
			return "error";
		
		int maxId = serAccess.getMaxId();
		Serialization toBeAdded = new Serialization(maxId + 1, name);

		serAccess.insert(toBeAdded);
		
		return "serialization";
	}
	
	
	@RequestMapping(value = "/addBook", method = RequestMethod.POST)
	public String addBook(@RequestParam("file") MultipartFile file, @ModelAttribute("detailedBook") DetailedBook detailedBook, Model model) {
		
		try {
			
			byte[] content = file.getBytes();
			Blob cover = new SerialBlob(content);
			
			int idBook = bookAccess.getMaxId();
			idBook++;
			
			int status = statusAccess.findByField("name", detailedBook.getStatus())
									 .get(0)
									 .getIdStatus();
			
			int author = authorAccess.findByField("name", detailedBook.getAuthor())
									 .get(0)
									 .getIdAuthor();
			
			int serialization = serAccess.findByField("name", detailedBook.getSerialization())
										 .get(0)
										 .getIdSerialization();
			
			bookAccess.addBook(new Book(
											idBook, 
											detailedBook.getName(), 
											detailedBook.getDescription(), 
											status, 
											author, 
											serialization, 
											detailedBook.getScore(),
											detailedBook.getNrOfUsers(),
											cover
										));
			
			for(String genre : detailedBook.getGenres()) {
				
				int idGenre = genresAccess.findByField("name", genre)
										  .get(0)
										  .getIdGenres();
				
				genresBook.insert(new BookGenres(idBook, idGenre));
			}
				
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SerialException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return "book";
	}
	
	
	@RequestMapping("/displayAll")
	public String toList(Model model) {
		
		List<DetailedBook> detailedBooks = bookAccess.getDetailedBooks();
		
		detailedBooks.forEach(book -> book.setScore(Float.valueOf(df.format(book.getScore()))));
		
		model.addAttribute("books", detailedBooks);
		
		return "displayList";
		
	}
	
	@RequestMapping("/home")
	public String toUserHome(HttpSession session, Model model) {
		
		int idUser = (int)session.getAttribute("userId");
		
		generateRecommendations(idUser, model);
		
		return "user";
	}
	
	@RequestMapping("/logOut")
	public String toIndex() {
		
		return "login";
	}
	
	@RequestMapping("/addAdmin")
	public String toAddAdmin(Model model, HttpSession session) {
		
		int userId = (int) session.getAttribute("userId");
		
		List<User> users = userAccess.selectAll()
									 .stream()
									 .filter(x -> !x.getIsAdmin() && x.getIdUser() != userId)
									 .collect(Collectors.toList());
		
		model.addAttribute("users", users);
		
		return "addAdmin";
	}
	
	@RequestMapping("/giveAdminRights")
	public String giveRights(@RequestParam("id") int idUser, HttpSession session, Model model) {
		
		userAccess.update("idUser", String.valueOf(idUser), "isAdmin", true);
		
		return toAddAdmin(model, session);
		
	}
	
	@RequestMapping("/removeAdmin")
	public String toRemoveAdmin(Model model, HttpSession session) {
		
		int userId = (int)session.getAttribute("userId");
		
		List<User> users = userAccess.selectAll()
				 .stream()
				 .filter(x -> x.getIsAdmin() && x.getIdUser() != userId)
				 .collect(Collectors.toList());

		model.addAttribute("users", users);
		
		return "removeAdmin";
	}
	
	@RequestMapping("/removeAdminRights")
	public String removeRights(@RequestParam("id") int idUser, Model model, HttpSession session) {
		
		userAccess.update("idUser", String.valueOf(idUser), "isAdmin", false);
		
		return toRemoveAdmin(model, session);
	}
	
	@RequestMapping("/homeAdmin")
	public String toHomeAdmin() {
		
		return "admin";
	}
	
	@RequestMapping("/moreDetails")
	public String toMoreInfo(@RequestParam("id") int idBook, HttpSession session, Model model) {
		
		DetailedBook detailedBook = bookAccess.getDetailedBook(idBook);
		
		String genres = genresAccess.getBookGenres(idBook);
		genres = genres.replace(",", ", ");
		
		detailedBook.setGenreList(genres);
		detailedBook.setScore(Float.valueOf(
											df.format(detailedBook.getScore())
											));
		
		model.addAttribute("detailedBook", detailedBook);
		session.setAttribute("idBook", idBook);
		
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		if(isAdmin)
			return "moreInfoAdmin";
		else 
			return "moreInfo";
	}
	
	@RequestMapping("/editBooks")
	public String toBooksDisplay(Model model) {
		
		List<DetailedBook> detailedBooks = bookAccess.getDetailedBooks();
		
		model.addAttribute("books", detailedBooks);
		
		return "displayBooksToEdit";
	}
	
	@RequestMapping("/editBook")
	public String toEditBook(@RequestParam("id") int idBook, Model model, HttpSession session) {
		
		DetailedBook detailedBook = bookAccess.getDetailedBook(idBook);
		detailedBook.setIdBook(idBook);
		
		model.addAttribute("detailedBook", detailedBook);
		session.setAttribute("originalBook", detailedBook);
		
		return "editBook";
		
	}
	
	@RequestMapping(value = "/updateTheBook", method = RequestMethod.POST)
	public String updateBook(@RequestParam("file") MultipartFile file, HttpSession session, @ModelAttribute("detailedBook") DetailedBook editedBook, Model model) {
		
		DetailedBook originalBook = (DetailedBook)session.getAttribute("originalBook");
		System.out.println(originalBook.getIdBook());
		
		try {
			
			if(file.getBytes().length == 0) {
				editedBook.setBlobCover(originalBook.getBlobCover());
			}else {
			
				byte[] content;		
				
				content = file.getBytes();
				Blob cover = new SerialBlob(content);
				
				editedBook.setBlobCover(cover);
			
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		int status = statusAccess.findByField("name", editedBook.getStatus())
				 .get(0)
				 .getIdStatus();

		int author = authorAccess.findByField("name", editedBook.getAuthor())
				 .get(0)
				 .getIdAuthor();

		int serialization = serAccess.findByField("name", editedBook.getSerialization())
					 .get(0)
					 .getIdSerialization();
		
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "cover", editedBook.getBlobCover());
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "name", editedBook.getName());
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "status", status);
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "author", author);
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "serialization", serialization);
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "score", editedBook.getScore());
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "nrofusers", editedBook.getNrOfUsers());
		bookAccess.update("idbook", String.valueOf(originalBook.getIdBook()), "description", editedBook.getDescription());
		
		model.addAttribute("detailedBook", editedBook);
		
		return "editBook";
	}
	
	@RequestMapping("/addToPlan")
	public String addToPlanToRead(HttpSession session, Model model) {
		
		int userId = (int)session.getAttribute("userId");
		int idBook = (int)session.getAttribute("idBook");
		
		System.out.println(idBook);
		
		userAccess.addToPlanToRead(userId, idBook);
		DetailedBook detailedBook = bookAccess.getDetailedBook(idBook);
		
		detailedBook.setScore(Float.valueOf(df.format(detailedBook.getScore())));
		model.addAttribute("detailedBook", detailedBook);
		
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		if(isAdmin)
			return "moreInfoAdmin";
		else
			return "moreInfo";
	}
	
	@RequestMapping("/addToRead")
	public String addToRead(@RequestParam("score") float score, HttpSession session, HttpServletRequest request, Model model) {
		
		int idUser = (int)session.getAttribute("userId");
		int idBook = (int)session.getAttribute("idBook");
		
		userAccess.addToRead(idUser, idBook, score);
		DetailedBook detailedBook = bookAccess.getDetailedBook(idBook);
		
		float oldScore = detailedBook.getScore();
		int nrOfUsers = detailedBook.getNrOfUsers();
		float newScore = (float) (((oldScore * nrOfUsers) + score) / (nrOfUsers + 1.0));
		
		bookAccess.update("idbook", String.valueOf(idBook), "score", newScore);
		bookAccess.update("idBook", String.valueOf(idBook), "nrOfUsers", nrOfUsers + 1);

		detailedBook.setScore(Float.valueOf(df.format(newScore)));
		detailedBook.setNrOfUsers(nrOfUsers + 1);
		
		model.addAttribute("detailedBook", detailedBook);
		
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		if(isAdmin)
			return "moreInfoAdmin";
		else
			return "moreInfo";
	}	
	
	@RequestMapping("/searchBy")
	public String searchBook(@RequestParam("content") String content, Model model, HttpSession session) {
		
		List<DetailedBook> books = bookAccess.getDetailedBooks();
		
		String lowerContent = content.toLowerCase();
		
		List<DetailedBook> filteredBooks = books.stream()
												.filter(book -> {
													
													return book.getName().toLowerCase().contains(lowerContent)
															|| book.getAuthor().toLowerCase().contains(lowerContent)
															|| book.getSerialization().toLowerCase().contains(lowerContent);
		
												})
												.collect(Collectors.toList());
		
		model.addAttribute("books", filteredBooks);
		
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		if(isAdmin)
			return "displayListAdmin";
		else
			return "displayList";
	}
	
	@RequestMapping("/planToReadList")
	public String displayPlan(HttpSession session, Model model) {
		
		int idUser = (int)session.getAttribute("userId");
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		List<DetailedBook> planToReadList = bookAccess.getBooksFromPlan(idUser);
		
		model.addAttribute("books", planToReadList);
		
		if(isAdmin)
			return "displayListAdmin";
		else
			return "displayList";
	}
	
	@RequestMapping("/readList")
	public String displayRead(HttpSession session, Model model) {
		
		int idUser = (int)session.getAttribute("userId");
		boolean isAdmin = (boolean)session.getAttribute("isAdmin");
		
		List<DetailedBook> readList = bookAccess.getReadBooks(idUser);
		
		model.addAttribute("books", readList);
		
		if(isAdmin)
			return "displayListAdmin";
		else
			return "displayList";
	}
}
