package com.javainterviewpoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.javainterviewpoint.Model.DetailedBook;
import com.javainterviewpoint.Recommendation.RecommendationSystem;
import com.javainterviewpoint.Recommendation.UserProfile;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class App extends SpringBootServletInitializer
{	
	/*@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
	{
		return builder.sources(AppConfig.class);
	}*/

	public static void main(String[] args)
	{
		SpringApplication.run(App.class);
		
		/*List<DetailedBook> dBooks = new ArrayList<DetailedBook>();
		DetailedBook book1 = new DetailedBook(0, "aven", "", "", "", 9, 0, null);
		book1.setGenres(Arrays.asList((new String[]{"superhero", "comedy", "sci-fi", "adventure"})));
		DetailedBook book2 = new DetailedBook(0, "spi", "", "", "", 7, 0, null);
		book2.setGenres(Arrays.asList((new String[]{"superhero", "adventure"})));
		DetailedBook book3 = new DetailedBook(0, "f4", "", "", "", 4, 0, null);
		book3.setGenres(Arrays.asList((new String[]{"superhero", "comedy", "sci-fi"})));
		
		dBooks.add(book1);
		dBooks.add(book2);
		dBooks.add(book3);
		
		UserProfile userProfile = (new RecommendationSystem()).generateUserProfile(dBooks);
	
		userProfile.getPreferences().keySet().forEach(key -> System.out.println(key + ": " + userProfile.getPrefenrece(key)));
	
		List<DetailedBook> dBooks2 = new ArrayList<DetailedBook>();
		DetailedBook book12 = new DetailedBook(0, "jl", "", "", "", 0, 0, null);
		book12.setGenres(Arrays.asList((new String[]{"superhero", "sci-fi", "adventure"})));
		DetailedBook book22 = new DetailedBook(0, "int", "", "", "", 0, 0, null);
		book22.setGenres(Arrays.asList((new String[]{"sci-fi", "adventure"})));
		DetailedBook book32 = new DetailedBook(0, "dark", "", "", "", 0, 0, null);
		book32.setGenres(Arrays.asList((new String[]{"superhero"})));
		
		dBooks2.add(book12);
		dBooks2.add(book22);
		dBooks2.add(book32);
		
		List<DetailedBook> result = (new RecommendationSystem()).generateRecommendations(dBooks2, userProfile);
	
		result.stream().forEach(book -> System.out.println(book.getName()));
		*/
	}
}
