package com.javainterviewpoint.Security;

/*import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import com.javainterviewpoint.DataAccess.DataSourceGenerator;
import com.javainterviewpoint.DataAccess.UserDAO;
import com.javainterviewpoint.Model.User;

@Service
public class AuthProvider implements AuthenticationProvider{

	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		System.out.println("hi");
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		UserDAO userAccess = new UserDAO(new DataSourceGenerator());
		
		User user = userAccess.getUser(username, password);
		
		if(user == null)
			throw new BadCredentialsException("Authentication failed");
		
		String role;
		
		if(user.getIsAdmin())
			role = "ROLE_ADMIN";
		else 
			role = "ROLE_USER";
		
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority(role));
		
		return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);
		
	}

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return true;
	}

}*/
