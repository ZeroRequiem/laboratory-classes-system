package com.javainterviewpoint.Recommendation;

import java.util.HashMap;
import java.util.List;

import com.javainterviewpoint.Model.DetailedBook;
import com.javainterviewpoint.Model.User;

public class UserProfile {
	
	//preference dependent on genre
	private HashMap<String, Float> preferences = new HashMap<String, Float>();
	
	public HashMap<String, Float> getPreferences() {
		return preferences;
	}

	public void setPreferences(HashMap<String, Float> preferences) {
		this.preferences = preferences;
	}
	
	public void addPreference(String genre, float value) {
		
		preferences.put(genre, value);
	}
	
	public float getPrefenrece(String genre) {
		
		if(!preferences.keySet().contains(genre))
			return 0.0f;
		
		return preferences.get(genre);
	}
}
