package com.javainterviewpoint.Recommendation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.javainterviewpoint.DataAccess.BookDAO;
import com.javainterviewpoint.Model.DetailedBook;

public class RecommendationSystem {
	
	public RecommendationSystem() {	
	}
	
	private HashMap<String, Float> generateBinaryRow(List<String> genres, float score, UserProfile userProfile) {
		
		HashMap<String, Float> binaryRow = new HashMap<String, Float>();
		
		genres.forEach(genre -> {
			
			binaryRow.put(genre, 1.0f * score);
			
			if(!userProfile.getPreferences().containsKey(genre))
				userProfile.addPreference(genre, 0.0f);
		});
		
		return binaryRow;
	}
	
	private UserProfile sumAndNormalization(UserProfile userProfile, HashMap<String, HashMap<String, Float>> books) {
		
		books.keySet().forEach(bookName ->{
			
			books.get(bookName).keySet().forEach(genre ->{
				
				float value;
				
				try {
					
					value = books.get(bookName).get(genre);
					userProfile.addPreference(genre, userProfile.getPrefenrece(genre) + value);
					
				}catch (NullPointerException e) {
					
					value = 0.0f;
					userProfile.addPreference(genre, userProfile.getPrefenrece(genre) + value);
				}

			});	
			
		});
		
		float maxValue = books.keySet().size() * 10;
		
		userProfile.getPreferences().keySet().forEach(genre ->{
			
			userProfile.addPreference(genre, userProfile.getPrefenrece(genre) / maxValue);
		});
		
		return userProfile;
	}
	
	public UserProfile generateUserProfile(List<DetailedBook> readBooks) {
		
		HashMap<String, HashMap<String, Float>> books = new HashMap<String, HashMap<String, Float>>();
		UserProfile userProfile = new UserProfile();
		
		if(readBooks.isEmpty())
			return userProfile;
		
		readBooks.forEach(book -> {
			
			books.put(book.getName(), generateBinaryRow(book.getGenres(), book.getScore(), userProfile));		
			
		});
		
		return sumAndNormalization(userProfile, books);
	}
	
	private HashMap<String, Float> generateBinaryRowForRes(List<String> genres, UserProfile userProfile) {
			
		HashMap<String, Float> binaryRow = new HashMap<String, Float>();
		
		genres.forEach(genre -> {
			
			binaryRow.put(genre, 1.0f * userProfile.getPrefenrece(genre));
		});
		
		return binaryRow;

	}
	
	private HashMap<String, Float> calcFinalWeights(HashMap<String, HashMap<String, Float>> books){
		
		HashMap<String, Float> weights = new HashMap<String, Float>();
		
		books.keySet().forEach(bookName -> {
			
			books.get(bookName).keySet().forEach(genre -> {
				
				float oldValue;
				
				try {
					
					oldValue = weights.get(bookName);
					
				}catch (NullPointerException e) {
					
					oldValue = 0.0f;
				}
				
				weights.put(bookName, oldValue + books.get(bookName).get(genre));
			});
		});
		
		return weights;
	}
	
	public List<DetailedBook> generateRecommendations(List<DetailedBook> allBooks, UserProfile userProfile){
		
		if(userProfile.getPreferences().keySet().isEmpty())
			return new ArrayList<>();
		
		HashMap<String, HashMap<String, Float>> books = new HashMap<String, HashMap<String, Float>>();
		
		allBooks.forEach(book -> {
			
			books.put(book.getName(), generateBinaryRowForRes(book.getGenres(), userProfile));		
			
		});
		
		HashMap<String, Float> weights = calcFinalWeights(books);
		
		return allBooks.stream()
					.sorted((book1, book2) -> {
						
						return weights.get(book2.getName())
										.compareTo(weights.get(book1.getName()));
					})
					.limit(10)
					.collect(Collectors.toList());
	}
}
