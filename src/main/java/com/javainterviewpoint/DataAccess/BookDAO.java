package com.javainterviewpoint.DataAccess;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import com.javainterviewpoint.Model.Book;
import com.javainterviewpoint.Model.DetailedBook;

public class BookDAO extends AbstractDAO<Book>{
	
	public BookDAO(DataSourceGenerator data) {
		
		super(data);
	}
	
	
	public void addBook(Book book) {
		
		insert(book);
	}
	
	public List<DetailedBook> getBooksFromPlan(int idUser){
		
		return getTemplate().query(
				"SELECT "
				+ "book.idBook as idBook, "
				+ "book.cover as blobCover, "
				+ "book.name as name, status.name as status, "
				+ "author.name as author, book.score as score, "
				+ "book.nrofusers as nrofusers, "
				+ "serialization.name as serialization\r\n" + 
				"FROM "
				+ "book inner join books.status on book.status=status.idstatus "
				+ "inner join author on book.author=author.idauthor "
				+ "inner join serialization on book.serialization=serialization.idserialization "
				+ "inner join plantoreadlist on book.idbook=plantoreadlist.idbook "
				+ "where iduser=" + idUser, 
				new BeanPropertyRowMapper<DetailedBook>(DetailedBook.class));
	}
	
	public List<DetailedBook> getReadBooks(int idUser){
		
		return getTemplate().query(
				"SELECT "
				+ "book.idBook as idBook, "
				+ "book.cover as blobCover, "
				+ "book.name as name, status.name as status, "
				+ "author.name as author, readlist.score as score, "
				+ "book.nrofusers as nrofusers, "
				+ "serialization.name as serialization\r\n" + 
				"FROM "
				+ "book inner join books.status on book.status=status.idstatus "
				+ "inner join author on book.author=author.idauthor "
				+ "inner join serialization on book.serialization=serialization.idserialization "
				+ "inner join readlist on book.idbook=readlist.idbook "
				+ "where iduser=" + idUser, 
				new BeanPropertyRowMapper<DetailedBook>(DetailedBook.class));
	}
	
	public List<DetailedBook> getUnreadBooks(UserDAO userAccess, int idUser){
		
		List<DetailedBook> detailedBooks = getDetailedBooks();
		
		return detailedBooks.stream()
							.filter(book -> {
								
								return userAccess.notInsideAnyList(idUser, book.getIdBook());
							})
							.collect(Collectors.toList());
	}
	
	public List<DetailedBook> getDetailedBooks(){
		
		return getTemplate().query(
					"SELECT "
					+ "book.idBook as idBook, "
					+ "book.cover as blobCover, "
					+ "book.name as name, status.name as status, "
					+ "author.name as author, book.score as score, "
					+ "book.nrofusers as nrofusers, "
					+ "serialization.name as serialization\r\n" + 
					"FROM "
					+ "book inner join books.status on book.status=status.idstatus "
					+ "inner join author on book.author=author.idauthor "
					+ "inner join serialization on book.serialization=serialization.idserialization;", 
					new BeanPropertyRowMapper<DetailedBook>(DetailedBook.class));
	}
	
	public DetailedBook getDetailedBook(int idBook){
		
		return getTemplate().query(
					"SELECT "
					+ "book.cover as blobCover, "
					+ "book.name as name, status.name as status, "
					+ "book.description as description,"
					+ "author.name as author, book.score as score, "
					+ "book.nrofusers as nrofusers, "
					+ "serialization.name as serialization\r\n" + 
					"FROM "
					+ "book inner join books.status on book.status=status.idstatus "
					+ "inner join serialization on book.serialization=serialization.idserialization "
					+ "inner join author on book.author=author.idauthor where idbook = " + idBook, 
					new BeanPropertyRowMapper<DetailedBook>(DetailedBook.class)).get(0);
	}
	
	public int getMaxId() {
		
		List<Book> res = getTemplate().query(
				"select * from book where idBook = (select max(idBook) from book)",
				new BeanPropertyRowMapper<Book>(Book.class)
				);
		
		if(res.size() == 0)
			return 0;
	
		return res.get(0)
				  .getIdBook();
	}
}
