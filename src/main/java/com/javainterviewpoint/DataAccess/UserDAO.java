package com.javainterviewpoint.DataAccess;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCallback;

import com.javainterviewpoint.Model.PlanToReadList;
import com.javainterviewpoint.Model.User;

public class UserDAO extends AbstractDAO<User>{
	
	public UserDAO(DataSourceGenerator data) {
		super(data);
	}
	
	public void addUser(User user) {
		
		insert(user);
	}
	
	public User getUser(String username, String password) {	
		
		List<User> users = getTemplate().query(
				"select * from `" + type.getSimpleName() + "` where name = '" + username + "' and password = '" + password + "'", 
				new BeanPropertyRowMapper<User>(User.class)
				);
		
		if(users.isEmpty())
			return null;
		else 
			return users.get(0);
	}
	
	public int getMaxId() {
		
		
		List<User> res = getTemplate().query(
				"select * from user where idUser = (select max(idUser) from user)",
				new BeanPropertyRowMapper<User>(User.class)
				);
		
		if (res.isEmpty())
			return 0;
		
		return res.get(0)
				  .getIdUser();
	}
	
	public boolean notInsideAnyList(int idUser, int idBook) {
		
		List<PlanToReadList> resPlan = getTemplate().query(
				"select * from plantoreadlist where idUser = " + idUser + " and idBook = " + idBook, 
				new BeanPropertyRowMapper<PlanToReadList>(PlanToReadList.class));

		
		
		return resPlan.isEmpty() && notInRead(idUser, idBook);
	}
	
	private boolean notInRead(int idUser, int idBook) {
		
		List<PlanToReadList> resRead = getTemplate().query(
				"select idUser, idBook from readlist where idUser = " + idUser + " and idBook = " + idBook, 
				new BeanPropertyRowMapper<PlanToReadList>(PlanToReadList.class));
	
		return resRead.isEmpty();
	}
	
	public boolean addToPlanToRead(int idUser, int idBook) {
		
		
		if(notInsideAnyList(idUser, idBook)) {
			
			String query = "insert into plantoreadlist values('" + idUser + "', '" + idBook + "')"; 
			
			return getTemplate().execute(query, new PreparedStatementCallback<Boolean>() {
				
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
					
					return ps.execute();
				}
			});
		}
		
		return false;
	}
	
	private boolean isInsidePlan(int idUser, int idBook) {
		
		List<PlanToReadList> resPlan = getTemplate().query(
				"select * from plantoreadlist where idUser = " + idUser + " and idBook = " + idBook, 
				new BeanPropertyRowMapper<PlanToReadList>(PlanToReadList.class));
	
		return !resPlan.isEmpty();
	}
	
	public boolean addToRead(int idUser, int idBook, float score) {
		
		if(isInsidePlan(idUser, idBook)) {
			
			String query = "delete from plantoreadlist where iduser = " + idUser + " and idbook = " + idBook; 
			
			getTemplate().execute(query, new PreparedStatementCallback<Boolean>() {
				
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
					
					return ps.execute();
				}
			});
		}
		
		if(notInRead(idUser, idBook)) {
		
			String query = "insert into readlist values('" + idBook + "', '" + idUser + "', '" + score +"')"; 
		
			return getTemplate().execute(query, new PreparedStatementCallback<Boolean>() {
			
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
				
					return ps.execute();
				}
			});
		}else 
			return false;
	}

}	


