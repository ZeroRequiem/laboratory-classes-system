package com.javainterviewpoint.DataAccess;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.javainterviewpoint.Model.Serialization;

public class SerializationDAO extends AbstractDAO<Serialization>{
	
	public SerializationDAO(DataSourceGenerator data) {
		
		super(data);
	}

	public int getMaxId() {
		
		
		List<Serialization> res = getTemplate().query(
				"select * from serialization where idserialization = (select max(idserialization) from serialization)",
				new BeanPropertyRowMapper<Serialization>(Serialization.class)
				);
		
		if (res.isEmpty())
			return 0;
		
		return res.get(0)
				  .getIdSerialization();
	}
	
}
