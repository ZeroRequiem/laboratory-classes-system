package com.javainterviewpoint.DataAccess;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DataSourceGenerator extends DriverManagerDataSource{
	
	private void setConnection() {
		
		this.setDriverClassName("com.mysql.cj.jdbc.Driver");
		this.setUrl("jdbc:mysql://localhost:3306/books?characterEncoding=latin1&useConfigs=maxPerformance");
		this.setUsername("root");
		this.setPassword("root");
	}
	
	public DataSourceGenerator() {
		
		super();
		setConnection();
	}
	
}
