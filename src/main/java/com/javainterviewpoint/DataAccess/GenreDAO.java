package com.javainterviewpoint.DataAccess;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.javainterviewpoint.Model.Genre;

public class GenreDAO extends AbstractDAO<Genre>{
	
	public GenreDAO(DataSourceGenerator data) {
		
		super(data);
	}

	public int getMaxId() {
		
		List<Genre> res = getTemplate().query(
				"select * from genre where idgenres = (select max(idgenres) from genre)",
				new BeanPropertyRowMapper<Genre>(Genre.class)
				);
		
		return res.get(0)
				  .getIdGenres();
	}
	
	public String getBookGenres(int idBook){
		
		return getTemplate().query(
				"SELECT group_concat(genre.name) as name \r\n" + 
				"FROM books.genre \r\n" + 
				"inner join bookgenres on genre.idgenres = bookgenres.idgenre \r\n" + 
				"inner join book on bookgenres.idbook = book.idbook \r\n" + 
				"where book.idbook = " + idBook,
				new BeanPropertyRowMapper<Genre>(Genre.class)
				).get(0)
				 .getName();

	}
	
	public List<String> getBookGenresAsList(int idBook){
		
		return getTemplate().query(
				"SELECT \r\n" + 
				"genre.name as name \r\n" + 
				"FROM\r\n" + 
				"books.genre inner join books.bookgenres on bookgenres.idgenre=genre.idgenres\r\n" + 
				"inner join books.book on book.idbook=bookgenres.idbook\r\n" + 
				"WHERE book.idbook= " + idBook,
				new BeanPropertyRowMapper<Genre>(Genre.class)
				).stream()
				 .map(genre -> genre.getName())
				 .collect(Collectors.toList());
	}

}
