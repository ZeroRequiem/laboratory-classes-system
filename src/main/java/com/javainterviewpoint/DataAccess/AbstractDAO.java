package com.javainterviewpoint.DataAccess;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

public class AbstractDAO<T> {
	
	protected final Class<T> type;
	private JdbcTemplate select;
	
	@SuppressWarnings("unchecked")
	public AbstractDAO(DataSourceGenerator data) {
		this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]; 
		this.select = new JdbcTemplate(data);
	}

	
	public JdbcTemplate getTemplate() {
		return select;
	}
	
	public List<T> findByField(String field, String value) {

		return select.query(
				"select * from `" + type.getSimpleName() + "` where " + field + " = '" + value + "'", 
				new BeanPropertyRowMapper<T>(type)
				);
	}
	
	public Boolean insert(T t) {
		
		String query = createInsertQuery(type.getDeclaredFields().length);
		Object[] values = new Object[type.getDeclaredFields().length];
		int index = 0;
		
		try {
		
			for (Field field: type.getDeclaredFields()) {
					
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
				Method method = propertyDescriptor.getReadMethod();
				Object value = method.invoke(t);
					
				values[index++] = value;

			}
			
			return select.execute(query, new PreparedStatementCallback<Boolean>() {
				
				@Override
				public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
					
					setStatement(ps, values);
					
					return ps.execute();
				}
			});
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return false;
	}
	
	public Boolean update(String identifier, String identifierValue, String field, Object newValue) {
		
		String query = createUpdateQuery(identifier, field);
		
		Object[] values = {newValue, identifierValue};
		
		return select.execute(query, new PreparedStatementCallback<Boolean>() {
			
			@Override
			public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {
				
				setStatement(ps, values);
				
				return ps.execute();
			}
		});
	}
	
	public List<T> selectAll(){
		
		return select.query(
							"select * from `" + type.getSimpleName() + "`", 
							new BeanPropertyRowMapper<T>(type)
							);
	}
	
    private String createInsertQuery(int nrOfValues){

        String buffer = "(";

        for(int i = 0; i < nrOfValues; i++){

            buffer += "?";

            if(i != nrOfValues - 1)
                buffer += ",";
        }

        buffer += ")";

        return "insert into `" + type.getSimpleName() + "` values" + buffer;
    }
    
    private String createUpdateQuery(String identifier, String field){ return "update `" + type.getSimpleName() + "` set " + field + "= ? where " + identifier +" = ?"; }

    
    void setStatement(PreparedStatement statement, Object[] values) throws SQLException{

        int paramIndex = 1;

        for (Object value : values) {//for all values
        	
            if (value instanceof Integer)//check what type of instance is to know what method to use to set the statement
                statement.setInt(paramIndex, Integer.parseInt(value.toString()));

            if (value.getClass().isAssignableFrom(String.class))
                statement.setString(paramIndex, value.toString());

            if (value instanceof Float)
                statement.setFloat(paramIndex, Float.parseFloat(value.toString()));

            if(value instanceof Boolean)
                statement.setBoolean(paramIndex, Boolean.parseBoolean(value.toString()));

            if(value instanceof Blob)
                statement.setBlob(paramIndex, (Blob) value);       

            paramIndex++;
        }
    }


}
