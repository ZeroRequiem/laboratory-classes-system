package com.javainterviewpoint.DataAccess;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.javainterviewpoint.Model.Author;

public class AuthorDAO extends AbstractDAO<Author>{
	
	public AuthorDAO(DataSourceGenerator data) {
		super(data);
	}
	
	public int getMaxId() {
		
		List<Author> res = getTemplate().query(
				"select * from author where idauthor = (select max(idauthor) from author)",
				new BeanPropertyRowMapper<Author>(Author.class)
				);
		
		if(res.size() == 0)
			return 0;
		
		return res.get(0)
				  .getIdAuthor();
	}

}
