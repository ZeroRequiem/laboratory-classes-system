package com.javainterviewpoint.Model;

import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;

public class DetailedBook {
	
	private int idBook;
	private String name;
	private String description;
	private String status;
	private String author;
	private String serialization;
	private float score;
	private int nrOfUsers;
	private Blob blobCover;
	List<String> genres;
	private String genreList;
	private byte[] cover;
	
	public DetailedBook(int idBook, String name, String status, String author, String serialization, float score,
			int nrOfUsers, byte[] cover) {
		this.idBook = idBook;
		this.name = name;
		this.status = status;
		this.author = author;
		this.serialization = serialization;
		this.score = score;
		this.nrOfUsers = nrOfUsers;
		this.cover = cover;
	}
	
	public DetailedBook() {
		genres = new ArrayList<String>();
	};
	
	public String getGenreList() {
		return genreList;
	}

	public void setGenreList(String genreList) {
		this.genreList = genreList;
	}

	public Blob getBlobCover() {
		return blobCover;
	}

	public void setBlobCover(Blob blobCover) {
		this.blobCover = blobCover;
	}

	
	public byte[] getCover() {
		return cover;
	}

	public void setCover(byte[] cover) {
		this.cover = cover;
	}

	public void setGenres(List<String> genres) {
		this.genres = genres;
	}
	
	public List<String> getGenres(){
		return genres;
	}
	
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSerialization() {
		return serialization;
	}
	public void setSerialization(String serialization) {
		this.serialization = serialization;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public int getNrOfUsers() {
		return nrOfUsers;
	}
	public void setNrOfUsers(int nrOfUsers) {
		this.nrOfUsers = nrOfUsers;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCoverUtility() throws UnsupportedEncodingException {
		
		
		try {
			
			byte[] encodedContent;
			encodedContent = Base64.encodeBase64(blobCover.getBytes(1, (int) blobCover.length()));
			
			String base64Encoded = new String(encodedContent, "UTF-8");
			
			return base64Encoded;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
}
