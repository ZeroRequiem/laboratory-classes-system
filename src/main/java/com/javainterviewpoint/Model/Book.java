package com.javainterviewpoint.Model;

import java.sql.Blob;
import java.sql.SQLException;

public class Book {
	
	private int idBook;
	private String name;
	private int status;
	private int author;
	private int serialization;
	private float score;
	private int nrOfUsers;
	private Blob cover;
	private String description;
	
	public Book(int idBook, String name, String description, int status, int author, int serialization, float score, int nrOfUsers, Blob cover) {
		
		this.idBook = idBook;
		this.name = name;
		this.description = description;
		this.status = status;
		this.author = author;
		this.serialization = serialization;
		this.score = score;
		this.nrOfUsers = nrOfUsers;
		this.cover = cover;
	}
	
	public Book() {
	}
	
	public byte[] getCoverData() throws SQLException {
		
		return cover.getBytes(1, (int) cover.length());
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Blob getCover() {
		return cover;
	}

	public void setCover(Blob cover) {
		this.cover = cover;
	}

	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getAuthor() {
		return author;
	}
	public void setAuthor(int author) {
		this.author = author;
	}
	public int getSerialization() {
		return serialization;
	}
	public void setSerialization(int serialization) {
		this.serialization = serialization;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public int getNrOfUsers() {
		return nrOfUsers;
	}
	public void setNrOfUsers(int nrOfUsers) {
		this.nrOfUsers = nrOfUsers;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
