package com.javainterviewpoint.Model;

public class Genre {
	
	private int idGenres;
	private String name;
	
	public Genre(int idGenres, String name) {
		this.idGenres = idGenres;
		this.name = name;
	}
	
	public Genre() { }
	

	public int getIdGenres() {
		return idGenres;
	}

	public void setIdGenres(int idGenres) {
		this.idGenres = idGenres;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
