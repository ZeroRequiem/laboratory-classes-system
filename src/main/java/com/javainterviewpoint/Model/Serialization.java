package com.javainterviewpoint.Model;

public class Serialization {
	
	private int idSerialization;
	private String name;
	
	public Serialization(int id, String name) {
		
		this.idSerialization = id;
		this.name = name;
	}
	
	public Serialization() {
	}
	
	public int getIdSerialization() {
		return idSerialization;
	}
	
	public void setIdSerialization(int id) {
		this.idSerialization = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
