package com.javainterviewpoint.Model;

public class BookGenres {

	private int idBook;
	private int idGnere;
	
	public BookGenres() {
		// TODO Auto-generated constructor stub
	}
	
	public BookGenres(int idBook, int idGnere) {
		this.idBook = idBook;
		this.idGnere = idGnere;
	}
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public int getIdGnere() {
		return idGnere;
	}
	public void setIdGnere(int idGnere) {
		this.idGnere = idGnere;
	}
	
}
