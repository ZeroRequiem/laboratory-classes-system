package com.javainterviewpoint.Model;

public class User {
	
	private int idUser;
	private String name;
	private String password;
	private boolean isAdmin;
	
	public User() {
	}
	
	public User(int idUser, String name, String password, boolean isAdmin) {
		
		this.idUser = idUser;
		this.name= name;
		this.password = password;
		this.isAdmin = isAdmin;
	}
	
	public int getIdUser() {
		
		return idUser;
	}
	
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
