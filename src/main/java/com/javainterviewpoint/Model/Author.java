package com.javainterviewpoint.Model;

public class Author {
	
	private int idAuthor;
	private String name;
	
	public Author(int idAuthor, String name) {
		
		this.idAuthor = idAuthor;
		this.name = name;
	}
	
	public Author() {}
	
	public int getIdAuthor() {
		return idAuthor;
	}
	
	public void setIdAuthor(int idAuthor) {
		this.idAuthor = idAuthor;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
