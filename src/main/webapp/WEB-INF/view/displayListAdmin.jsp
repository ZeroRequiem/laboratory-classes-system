<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri ="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body  
{  
    margin: 0;  
    padding: 0;  
    background-color:#6abadeba;  
    font-family: Arial, Helvetica, sans-serif;
    background-image: url('https://wallpapercave.com/wp/wp2036988.png');
    background-repeat: no-repeat;
  	background-attachment: fixed;
  	background-size: cover;
}  

.navbar {
  overflow: hidden;
  background-color: #333;
}

.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

tr:nth-child(even) {
	background-color: lightgray;
}

tr:nth-child(odd) {
	background-color: lightblue;
}

th {
	text-align: left;
}

td {
	text-align: center;
}

.panel {

	margin-left: auto;
	margin-right: auto;
}

.navbar .searchCnt {
  
  float: right;
  padding: 14px 16px;
}

#textA{  
    width: 200px;  
    height: 25px;  
    border: none;  
    border-radius: 3px;  
    padding-left: 8px;  
}  

</style>
</head>
<body>

<div class="navbar">
  <a href="/homeAdmin">Home</a>
  <a href="/addAdmin">Add admin</a>
  <a href="/removeAdmin">Remove admin</a>
  <div class="dropdown">
    <button class="dropbtn">Options<i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="/author">Add author</a>
      <a href="/serialization">Add serialization</a>
      <a href="/genre">Add new genre</a>
      <a href="/book">Add book</a>
      <a href="/logOut">Log Out</a>
    </div>
  </div>
  <div class="dropdown">
    <button class="dropbtn">Edit<i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="/editBooks">Edit book</a>
    </div>
  </div>  
    <div class="searchCnt">
  <form action="searchBy">
  	<input type="text" name="content" id="textA" placeholder="Search">
  	&nbsp;&nbsp;&nbsp;
  	<input type="submit" id="btn" value="Search">
  </form> 
  </div>
</div>

<table class="panel" cellpadding="10">
		<tr>
			<th>Cover</th>
			<th>Title</th>
			<th>Author</th>
			<th>Serialization</th>
			<th>Status</th>
			<th>Score</th>
			<th>Number of Users</th>
		</tr>
		<c:forEach items="${books}" var="book">
			<tr>
				<td>
					<c:url value="/moreDetails" var="more"/>
					<a href="${more}?id=${book.getIdBook()}">
					<img width="60" height="80" src="data:image/jpeg;base64,${ book.getCoverUtility() }"/>
					</a>
				</td>
				<td>${book.getName()}</td>
				<td>${book.getAuthor()}</td>
				<td>${book.getSerialization()}</td>
				<td>${book.getStatus()}</td>
				<td>${book.getScore()}</td>
				<td>${book.getNrOfUsers()}</td>
			</tr>
		</c:forEach>
</table>

</body>
</html>