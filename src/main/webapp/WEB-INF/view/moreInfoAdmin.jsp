<%@page import="com.javainterviewpoint.Model.DetailedBook"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>

<style>
.container {
   margin-left: auto; /* this will center the page */
   margin-right: auto;
   background-color: lightblue;
   padding: 20px;
   width:60%; /*  use your width here */
}

.title {
	width: 900px;  
    height: 100px;  
    border: none;  
    border-radius: 17px;  
    padding-left: 7px;  
    color: black;
    font-weight: 800;  
}

body  
{  
    margin: 0;  
    padding: 0;  
    background-color:#6abadeba;  
    font-family: Arial, Helvetica, sans-serif;
    background-image: url('https://wallpapercave.com/wp/wp2036988.png');
    background-repeat: no-repeat;
  	background-attachment: fixed;
  	background-size: cover;
}  

.navbar {
  overflow: hidden;
  background-color: #333;
}

.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

div.b {
  word-wrap: break-word;
}

label{  
    width: 500px;  
    height: 50px;  
    border: none;  
    border-radius: 17px;  
    padding-left: 7px;  
    color: black;
    font-weight: 800;  
}  

.navbar .searchCnt {
  
  float: right;
  padding: 14px 16px;
}

#btn{  
    width: 200px;  
    height: 30px;  
    border: none;  
    border-radius: 17px;  
    padding-left: 7px;  
    color: black;
    font-weight: 600;    
}

#textA{  
    width: 200px;  
    height: 25px;  
    border: none;  
    border-radius: 3px;  
    padding-left: 8px;  
}  

</style>

<body>

<div class="navbar">
  <a href="/homeAdmin">Home</a>
  <a href="/addAdmin">Add admin</a>
  <a href="/removeAdmin">Remove admin</a>
  <div class="dropdown">
    <button class="dropbtn">Options<i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="/author">Add author</a>
      <a href="/serialization">Add serialization</a>
      <a href="/genre">Add new genre</a>
      <a href="/book">Add book</a>
      <a href="/logOut">Log Out</a>
    </div>
  </div>
  <div class="dropdown">
    <button class="dropbtn">Edit<i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="/editBooks">Edit book</a>
    </div>
  </div>  
    <div class="searchCnt">
  <form action="searchBy">
  	<input type="text" name="content" id="textA" placeholder="Search">
  	&nbsp;&nbsp;&nbsp;
  	<input type="submit" id="btn" value="Search">
  </form> 
  </div>
</div>
<br>
<br>
<div class="container">
<table style="width: 100%">
	<colgroup>
		<col span="1" style="width: 225px">
		<col span="1" style="width: 799px">
	</colgroup>
	
	<tbody>
		<tr>
			<td>
				<img width="225" height="324" src="data:image/jpeg;base64,${ detailedBook.getCoverUtility() }"/>
				<br>
				<br>
				<div class="b" style="width: 225px">
					${detailedBook.getGenreList()}
					<br><br>
					Author: ${ detailedBook.getAuthor() }
					<br><br>
					Serialization: ${ detailedBook.getSerialization() }
				</div>
				
			</td>
			<td style="vertical-align:top; padding-top: 20px; padding-left: 20px">
				<div>
					<label class="title">${ detailedBook.getName() }</label>
				</div>
				<br>
				<br>
				<div>
					<label>Score: ${ detailedBook.getScore() }</label>
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<label>Users: ${ detailedBook.getNrOfUsers() }</label>
				</div>
				<br>
				<form action="/addToPlan">
					<input type="hidden" name="idBook" value="${detailedBook.getIdBook()}">
					<input type="submit" value="Add To Plan To Read" id = btn>
				</form>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<form action="/addToRead">
					<input type="submit" value="Add To Read" id = btn>
					<input type="number" id="textA" step="0.01" min="0.0" max="10.0" name="score" placeholder="Your score"/>
					
				</form>
				<br>
				<div class="b">
					${ detailedBook.getDescription() }
				</div>
			</td> 
		</tr>
	</tbody>
</table>

</div>
</body>
</html>