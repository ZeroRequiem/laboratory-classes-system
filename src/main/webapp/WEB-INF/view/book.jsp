<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri ="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body  
{  
    margin: 0;  
    padding: 0;  
    background-color:#6abadeba;  
    font-family: Arial, Helvetica, sans-serif;
    background-image: url('https://wallpapercave.com/wp/wp2036988.png');
    background-repeat: no-repeat;
  	background-attachment: fixed;
  	background-size: cover;
}  

.navbar {
  overflow: hidden;
  background-color: #333;
}

.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}



.panel{  
        width: 382px;  
        overflow: hidden;  
        margin: auto;  
        margin: 20 0 0 450px;  
        padding: 80px;  
        top: 100px;
        background: #9F9F9F;  
        border-radius: 15px ;  
          
}  

#fname{  
    width: 300px;  
    height: 30px;  
    border: none;  
    border-radius: 3px;  
    padding-left: 8px;  
}  

label{  
    color: #08ffd1;  
    font-size: 17px;  
}  

#btn{  
    width: 300px;  
    height: 30px;  
    border: none;  
    border-radius: 17px;  
    padding-left: 7px;  
    color: black;
    font-weight: 600;    
}  

.navbar .searchCnt {
  
  float: right;
  padding: 14px 16px;
}

#textA{  
    width: 200px;  
    height: 25px;  
    border: none;  
    border-radius: 3px;  
    padding-left: 8px;  
}  

</style>
<body>

<div class="navbar">
  <a href="/homeAdmin">Home</a>
  <a href="/addAdmin">Add admin</a>
  <a href="/removeAdmin">Remove admin</a>
  <div class="dropdown">
    <button class="dropbtn">Options<i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="/author">Add author</a>
      <a href="/serialization">Add serialization</a>
      <a href="/genre">Add new genre</a>
      <a href="/book">Add book</a>
      <a href="/logOut">Log Out</a>
    </div>
  </div> 
    <div class="searchCnt">
  <form action="searchBy">
  	<input type="text" name="content" id="textA" placeholder="Search">
  	&nbsp;&nbsp;&nbsp;
  	<input type="submit" id="btn" value="Search">
  </form> 
  </div>
</div>
<br><br>
<div class="panel">
<h2>Add Book</h2>
<form:form method="post" action="addBook" modelAttribute="detailedBook" enctype="multipart/form-data">
  <label>Cover:</label>
  <input type="file" name="file">
  <Label>${cover.getOriginalFilename()}</Label>
  <br><br>
  <form:label path="name">Title:</form:label><br>
  <form:input type="text" id="fname" path="name"/><br>
  <br><br>
  <form:label path="description">Description:</form:label>
  <form:textarea id="fname" path="description" rows="100" col="50"/><br>
  <br><br>
  <form:label path="score">Score:</form:label><br>
  <form:input type="text" id="fname" path="score"/><br>
  <br><br>
  <form:label path="nrOfUsers">Number of users:</form:label><br>
  <form:input type="text" id="fname" path="nrOfUsers"/><br>
  <table>
  <tr>
	<th colspan="3" id="fname">Genres</th>
  </tr>
  <tr>
  	<td>
  	<form:checkboxes items="${genresList}" path="genres"/>
  	</td>
  </tr>
  </table>
  <br><br>
  <form:select path="status">
  	<form:options items ="${ statusList }"/>
  </form:select>
  <br><br>
  <form:select path="author">
  	<form:options items="${ authorList }"></form:options>
  </form:select>
  <br><br>
  <form:select path="serialization">
  	<form:options items="${ serList }"></form:options>
  </form:select>
  <br><br>
  <input type="submit" value="Submit" id = btn>
</form:form>
</div>

</body>
</html>